<!doctype html>
<html lang="en">

    {% include head.tmpl %}

    <body>

        <main class="reveal">

            <div class="slides">

                <header class="frontpage" id="slide-header">

                    {% include logo-brand.svg %}

                    {% if page.title %}
                    <h1 id="title">{{ page.title }}</h1>
                    {% else %}
                    <h1 id="title">{{ site.title }}</h1>
                    {% endif %}

                    
                </header>
                <div
                <div class="frontpage box" id="first-slide">
                    <h1 ><strong>{{ site.firstslide-title }}</strong></h3>
                    <h2 >{{ site.firstslide-subtitle }}</h3>
                    <h3 >{{ site.firstslide-author }}</h3>
                </div>
                <div class="frontpage bg" id="first-slide-bg">

                </div>
                {% for post in site.posts reversed %}
                
                <section data-markdown data-separator="^\n---\n$" data-separator-vertical="^\n--\n$" data-notes="^Note:">
					<textarea data-template>
{%
						assign lines = post.content | newline_to_br | strip_newlines | split: "<br />"
%}{%
						for line in lines
%}{%
							assign processed_line = line
								| replace:'<fragment/>','<!-- .element: class="fragment" -->'
								| replace:'<background>','<!-- .slide: data-background="'
								| replace:'</background>','" -->'
								| replace:'<backgroundimage>','<!-- .slide: data-background="'
								| replace:'</backgroundimage>','" --> '
								| replace:'<backgroundimageopacity>','<!-- .slide: data-background-opacity="'
								| replace:'</backgroundimageopacity>','" -->'
								| replace:'<slide-id>','<!-- .slide: id="'
								| replace:'</slide-id>','" --> '
								| replace:'<slidevisibility>','<!-- .slide: data-visibility="'
								| replace:'</slidevisibility>','" --> '
								| replace:'<auto-animate/>','<!-- .slide: data-auto-animate -->'
%}{%
							assign first_char = line | strip
								| slice: 0,1
%}{%
							if first_char == '+'
%}{%
								assign processed_line = processed_line
									| replace_first: '+','+ <!-- .element: class="fragment" -->'
%}{%
							endif
							%}{{ processed_line }}{% comment %}Following line break is important{% endcomment %}
{%
						endfor
%}
					</textarea>

                </section>
                {% endfor %}
            </div>
            <div class="slide-number impressum" style="display: block;display">
                <a href="https://www.ioer.de/impressum">Impressum </a> |
                <a href="https://www.ioer.de/datenschutz">Datenschutz </a> |
                <a href="https://www.ioer.de/barrierefreiheit">Barrierefreiheit </a>
            </div>
        </main>

        <script src="{{ '/assets/vendor/reveal.js/dist/reveal.js' | relative_url }}"></script>
        <script src="{{ '/assets/vendor/reveal.js/plugin/markdown/markdown.js' | relative_url }}"></script>
        <script src="{{ '/assets/vendor/reveal.js/plugin/zoom/zoom.js' | relative_url }}"></script>
        <script src="{{ '/assets/vendor/reveal.js/plugin/highlight/highlight.js' | relative_url }}"></script>
        <script src="{{ '/assets/vendor/reveal.js/plugin/math/math.js' | relative_url }}"></script>
        <script src="{{ '/assets/vendor/reveal.js/plugin/notes/notes.js' | relative_url }}"></script>

        <script>
            let r = Reveal();
			r.initialize({
                controls: {% if site.controls != null %}{{ site.controls }}{% else %}true{% endif %},
                progress: {% if site.progress != null %}{{ site.progress }}{% else %}true{% endif %},
                center: {% if site.progress != null %}{{ site.progress }}{% else %}true{% endif %},
                history: true,
                pdfSeparateFragments: false,
                slideNumber: 'c/t',
                width: {{ site.width | default: 1280 }},
                height: {{ site.height | default: 700 }},

				transition: '{{ site.transition | default: none }}',

                plugins: [ RevealMarkdown, RevealHighlight, RevealNotes, RevealZoom, RevealMath ],

                dependencies: [
                {% if site.reveal_dependencies %}
				{{ site.reveal_dependencies }}
                {% endif %}
                ]
            });
        </script>
        <script type="text/javascript">

            function toggleLogoClasses(){
                var logoClasses = document.getElementById("slide-header").classList;
                var frontClasses = document.getElementById("first-slide").classList;
                var frontBgClasses = document.getElementById("first-slide-bg").classList;
                if ( r.isFirstSlide() ){
                    logoClasses.add("frontpage");
                    frontClasses.add("frontpage");
                    frontBgClasses.add("frontpage");
                } else {
                    logoClasses.remove("frontpage");
                    frontClasses.remove("frontpage");
                    frontBgClasses.remove("frontpage");
                }
            }

            r.on( 'ready', function() {
                toggleLogoClasses();
            });

            r.on( 'slidechanged', function() {
                toggleLogoClasses();
            });

        </script>
    </body>
</html>
