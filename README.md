# Jekyll Theme Gem for Presentations

**If you simply want to create a presentation within the VGIscience domain using this theme, just go ahead and fork [the Presentation Template](https://gitlab.hrz.tu-chemnitz.de/ioer/common/presentation) and follow the instructions in that project's `README.md`. It already utilizes this theme gem.**

The following is only of interest for the integration in remote Jekyll instances.

## Installation

Add this line to your Jekyll site's `Gemfile`:

```ruby
gem "ioer-presentation-theme", :git => "git@gitlab.hrz.tu-chemnitz.de:ioer/common/web-presentation-theme.git"
```

And add this line to your Jekyll site's `_config.yml`:

```yaml
theme: ioer-presentation-theme
```

And then execute:

```shell
bundle update
```
