# coding: utf-8

Gem::Specification.new do |spec|
  spec.name          = "ioer-presentation-theme"
  spec.version       = "0.1.0"
  spec.authors       = ["Alexander Dunkel"]
  spec.email         = ["alexander.dunkel@tu-dresden.de"]

  spec.summary       = "A jekyll theme for IOER-styled HTML presentation slides"
  spec.homepage      = "https://gitlab.vgiscience.de/ad/presentation-theme-2024"
  spec.license       = "BSD-2-Clause"

  spec.files         = `git ls-files -z`.split("\x0").select { |f| f.match(%r{^(assets|_layouts|_includes|_sass|LICENSE|README)}i) }

  spec.add_runtime_dependency "jekyll", "~> 3.7"

  spec.add_development_dependency "bundler", "~> 1.16"
  spec.add_development_dependency "rake", "~> 12"
end
